import ApiResponse from "../../helpers/ApiResponse";
import Booking from "../../models/booking/booking.model";
import { checkExistThenGet} from "../../helpers/CheckMethods";
import {checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
const populateQuery = [
    { path: 'train', model: 'train' },
    { path: 'user', model: 'user' }
];
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {userId,trainId } = req.query;
            let query = {deleted: false };
            if (userId) query.user = userId;
            if (trainId) query.train = trainId;
            let Bookings = await Booking.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const BookingCount = await Booking.count(query);
            const pageCount = Math.ceil(BookingCount / limit);

            res.send(new ApiResponse(Bookings, page, pageCount, limit, BookingCount, req));
        } catch (err) {
            next(err);
        }
    },
   
    validateBody(isUpdate = false) {
        let validations = [
            body('train').not().isEmpty().withMessage('train is required'),
            body('user').not().isEmpty().withMessage('user id require')
        ];

        return validations;
    },

    async create(req, res, next) {

        try {
            const validatedBody = checkValidations(req);
            let createdBooking = await Booking.create({ ...validatedBody});

            
            res.status(201).send(createdBooking);
        } catch (err) {
            next(err);
        }
    },
   
    async delete(req, res, next) {
        try {
            let {  bookingId } = req.params;
            let  Book = await checkExistThenGet( bookingId,  Booking, { deleted: false });
            Book.deleted = true;
            await  Book.save();
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};