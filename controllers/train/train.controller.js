import ApiResponse from "../../helpers/ApiResponse";
import Train from "../../models/train/train.model";
import Booking from "../../models/booking/booking.model";
import { checkExist, checkExistThenGet} from "../../helpers/CheckMethods";
import {checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false };
            let trains = await Train.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const trainsCount = await Train.count(query);
            const pageCount = Math.ceil(trainsCount / limit);

            res.send(new ApiResponse(trains, page, pageCount, limit, trainsCount, req));
        } catch (err) {
            next(err);
        }
    },
   
    validateBody(isUpdate = false) {
        let validations = [
            body('from').not().isEmpty().withMessage('from is required'),
            body('to').not().isEmpty().withMessage('to is required'),
            body('date').not().isEmpty().withMessage('date is required'),
               
        ];

        return validations;
    },

    async create(req, res, next) {

        try {
            const validatedBody = checkValidations(req);
            let createdTrain = await Train.create({ ...validatedBody});

            
            res.status(201).send(createdTrain);
        } catch (err) {
            next(err);
        }
    },
    async findById(req, res, next) {
        try {
            let { trainId } = req.params;
            await checkExist(trainId, Train, { deleted: false });
            let train = await Train.findById(trainId);
            res.send(train);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let { trainId } = req.params;
            await checkExist(trainId, Train, { deleted: false });

            const validatedBody = checkValidations(req);
            let updatedTrain = await Train.findByIdAndUpdate(trainId, {
                ...validatedBody,
            }, { new: true });
            res.status(200).send(updatedTrain);
        }
        catch (err) {
            next(err);
        }
    },
   
    async delete(req, res, next) {
        try {
            let { trainId } = req.params;
            let train = await checkExistThenGet(trainId, Train, { deleted: false });
            let reservs = await Booking.find({ train: trainId });
            for (let reserv of reservs ) {
                reserv.deleted = true;
                await reserv.save();
            }
            train.deleted = true;
            await train.save();
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};