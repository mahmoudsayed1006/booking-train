import {checkExist } from '../../helpers/CheckMethods';
import { body } from 'express-validator/check';
import { checkValidations} from '../shared/shared.controller';
import { generateToken } from '../../utils/token';
import User from "../../models/user/user.model";

export default {
    async signIn(req, res, next) {
        try{
            let user = req.user;
           
            user = await User.findById(user.id),
                res.status(200).send({
                    user,
                    token: generateToken(user.id)
                });
        } catch(err){
            next(err);
        }
    },

    validateUserCreateBody(isUpdate = false) {
        let validations = [
            body('country').not().isEmpty().withMessage('country is required'),
            body('birthday').not().isEmpty().withMessage('birthday is required'),
            body('username').not().isEmpty().withMessage('username is required')
                .custom(async (value, { req }) => {
                    let userQuery = { username: value };
                    if (isUpdate && req.user.username === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('username duplicated'));
                    else
                        return true;
                }),
            body('phone').not().isEmpty().withMessage('phone is required')
                .custom(async (value, { req }) => {
                    let userQuery = { phone: value };
                    if (isUpdate && req.user.phone === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('phone duplicated'));
                    else
                        return true;
                }),
            body('email').not().isEmpty().withMessage('email is required')
                .isEmail().withMessage('email syntax')
                .custom(async (value, { req }) => {
                    let userQuery = { email: value };
                    if (isUpdate && req.user.email === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
                }),

                body('type')
        ];
        if (!isUpdate) {
            validations.push([
                body('password').not().isEmpty().withMessage('password is required')
            ]);
        }
        return validations;
    },
    async signUp(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let createdUser = await User.create({
                ...validatedBody
            });
            res.status(201).send({
                user: await User.findOne(createdUser),
                token: generateToken(createdUser.id)
            });

        } catch (err) {
            next(err);
        }
    },
    
    async findById(req, res, next) {
        try {
            let { id } = req.params;
            await checkExist(id, User, { deleted: false });

            let user = await User.findById(id);
            res.send(user);
        } catch (error) {
            next(error);
        }
    },


};
