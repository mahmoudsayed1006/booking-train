import express from 'express';
import { requireSignIn, requireAuth } from '../../services/passport';
import UserController from '../../controllers/user/user.controller';

const router = express.Router();


router.post('/signin', requireSignIn, UserController.signIn);

router.route('/signup')
    .post(
        UserController.validateUserCreateBody(),
        UserController.signUp
    );

export default router;
