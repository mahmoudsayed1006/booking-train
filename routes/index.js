import express from 'express';
import userRoute from './user/user.route';
import TrainRoute  from './train/train.route';
import BookingRoute  from './booking/booking.route';

const router = express.Router();

router.use('/', userRoute);
router.use('/dates', TrainRoute);
router.use('/booking', BookingRoute);
export default router;
