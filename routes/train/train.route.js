import express from 'express';
import TrainController from '../../controllers/train/train.controller';

const router = express.Router();

router.route('/')
    .post(
        TrainController.validateBody(),
        TrainController.create
    )
    .get(TrainController.findAll);

router.route('/:trainId')
    .put(
        TrainController.validateBody(true),
        TrainController.update
    )
    .get(TrainController.findById)
    .delete( TrainController.delete);


export default router;