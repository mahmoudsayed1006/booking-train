import express from 'express';
import BookingController from '../../controllers/booking/booking.controller';

const router = express.Router();

router.route('/')
    .post(
        BookingController.validateBody(),
        BookingController.create
    )
    .get(BookingController.findAll);

router.route('/:bookingId')
    .delete(BookingController.delete);


export default router;