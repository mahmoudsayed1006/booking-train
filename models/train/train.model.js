import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const TrainSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    from: {
        type: String,
        trim: true,
        required: true,
    },
    to: {
        type: String,
        trim: true,
        required: true,
    },
    date:{
        type:Date,
        required:true
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

TrainSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
TrainSchema.plugin(autoIncrement.plugin, { model: 'train', startAt: 1 });

export default mongoose.model('train', TrainSchema);