import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const BookingSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    train: {
        type: Number,
        required: true,
        ref:'train'
    },
    user:{
        type:Number,
        required:true,
        ref:'user'
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

BookingSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
BookingSchema.plugin(autoIncrement.plugin, { model: 'booking', startAt: 1 });

export default mongoose.model('booking', BookingSchema);